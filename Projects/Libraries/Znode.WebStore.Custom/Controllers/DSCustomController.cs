﻿using System.Diagnostics;
using System.Web.Mvc;
using Znode.Engine.WebStore.Controllers;
using Znode.Libraries.Framework.Business;
using Znode.WebStore.Custom.Agents.Agents;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.Controllers
{
    public class DSCustomController : BaseController
    {
        private readonly IDSCustomAgent _DSCustomAgent;

        public DSCustomController()
        {
            _DSCustomAgent = new DSCustomAgent();
        }

        [HttpGet]
        public virtual ActionResult GetInventoyGrid(int PublishProductId, string Color)
        {
            DSCustomListViewModel list = _DSCustomAgent.GetInventoyGrid(PublishProductId, Color);
            ZnodeLogging.LogMessage("Webstore controller-", TraceLevel.Error.ToString());
            return PartialView("../Customized/_DSPdpGrid", list);

        }
    }
}
