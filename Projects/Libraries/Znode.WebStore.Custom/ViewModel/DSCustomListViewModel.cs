﻿using System.Collections.Generic;
using Znode.Engine.WebStore;
namespace Znode.WebStore.Custom.ViewModel
{
    public class DSCustomListViewModel : BaseViewModel
    {
        public List<DSCustomViewModel> InventoryList { get; set; }
    }
}
