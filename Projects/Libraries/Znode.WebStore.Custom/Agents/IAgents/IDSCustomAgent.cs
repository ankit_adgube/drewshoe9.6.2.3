﻿
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.Agents.IAgents
{
    interface IDSCustomAgent
    {
        //string GetInventoyGrid(int PublishProductId, string Color);
        DSCustomListViewModel GetInventoyGrid(int PublishProductId, string Color);
    }
}
