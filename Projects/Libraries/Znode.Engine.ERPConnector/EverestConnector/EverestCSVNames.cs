﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.ERPConnector.EverestConnector
{
    public class EverestCSVNames
    {
        //Inventory
        public const string InventoryBySKU = "MAGINV";
        //Pricing
        public const string Pricing = "TPRICE";
    }
}
