﻿using MongoDB.Bson.IO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using Znode.Api.Client.Custom.Endpoints.Custom;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Api.Client.Custom.Clients.IClients;
using Znode.Sample.Api.Model.Responses;
using Znode.Sample.Api.Model;
using Znode.Engine.Api.Models.Extensions;

namespace Znode.Api.Client.Custom.Clients.Clients
{
    public class DSCustomClient : BaseClient, IDSCustomClient
    {

        public virtual DSCustomListModel GetInventoyGrid(int PublishProductId, string Color)
        {
            FilterCollection filters = null;
            ExpandCollection expands = null;
            string endpoint = DSCustomEndpoints.GetInventoyGrid(PublishProductId, Color);
            endpoint += BuildEndpointQueryString(expands);
            ApiStatus status = new ApiStatus();

            DSCustomResponse response = GetResourceFromEndpoint<DSCustomResponse>(endpoint, status);
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            DSCustomListModel list = new DSCustomListModel { InventoryList = response?.InventoryList };
            list.MapPagingDataFromResponse(response);
            return list;
        }



    }
}
