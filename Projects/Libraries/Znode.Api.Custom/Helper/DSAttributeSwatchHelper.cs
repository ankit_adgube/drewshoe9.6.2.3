﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.ECommerce.Utilities;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
namespace Znode.Api.Custom.Helper
{
    public class DSAttributeSwatchHelper : AttributeSwatchHelper
    {
        string PortalIDDealer = ConfigurationManager.AppSettings["PortalIDDealer"];

        public override string GetAttributeSwatchCodeExapnds(NameValueCollection expands)
        {
            string swatchAttributeCode = "";
            foreach (string key in expands.Keys)
            {
                string value = expands.Get(key);
                if (value.StartsWith(ExpandKeys.Swatch_AttributeCode))
                {
                    //Here we will get the value in swatch_yourattributecode or value we will split it with the _ and get the value.
                    swatchAttributeCode = value.Split('_')[1];
                    return swatchAttributeCode;
                }
                return swatchAttributeCode;
            }
            return swatchAttributeCode;
        }
        public override void GetAssociatedConfigurableProducts(KeywordSearchModel searchModel, SearchRequestModel model, string attributeCode)
        {
            string drewcode = null;
            IImageHelper imageHelper = GetService<IImageHelper>();

            foreach (SearchProductModel searchProduct in searchModel.Products)
            {
                string productType = ZnodeConstant.ConfigurableProduct;
                List<AttributesSelectValuesModel> selectValues = null;

                if (IsConfigurableProduct(searchProduct, productType))
                {
                    if(model.PortalId == Convert.ToInt32(PortalIDDealer))
                    {
                        drewcode = searchProduct.Attributes?.Where(y => y.AttributeCode == "drewcolor")?.FirstOrDefault()?.AttributeName;

                        if(drewcode != null)
                        {
                            selectValues = GetSelectValues("drewcolor", searchProduct);
                        }
                        else
                        {
                            selectValues = GetSelectValues("roscolor", searchProduct);
                        }
                    }
                    else
                    {                       
                        selectValues = GetSelectValues(attributeCode, searchProduct);
                    }
                   
                    if (selectValues?.Count > 0)
                    {                       
                        searchProduct.SwatchAttributesValues = GetSwatchImages(selectValues, imageHelper);
                       // string ImageSmallPath = imageHelper.GetImageHttpPathSmall(selectValues.First().VariantImagePath);
                        //nivi Code change 
                        searchProduct.ImageSmallPath = imageHelper.GetImageHttpPathSmall(searchProduct.Attributes.Where(y => y.AttributeCode == ZnodeConstant.ProductImage)?.FirstOrDefault()?.AttributeValues);
                    }
                    else
                        searchProduct.ImageSmallPath = imageHelper.GetImageHttpPathSmall(searchProduct.Attributes.Where(y => y.AttributeCode == ZnodeConstant.ProductImage)?.FirstOrDefault()?.AttributeValues);
                }
                else
                    searchProduct.ImageSmallPath = imageHelper.GetImageHttpPathSmall(searchProduct.Attributes.Where(y => y.AttributeCode == ZnodeConstant.ProductImage)?.FirstOrDefault()?.AttributeValues);
            }
        }        
        public override bool IsConfigurableProduct(SearchProductModel searchProduct, string productType)
        {
            return searchProduct.Attributes.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.ProductType)?.SelectValues.FirstOrDefault().Code == productType;
        }
        public override List<WebStoreAttributeValueSwatchModel> GetSwatchImages(List<AttributesSelectValuesModel> swatches, IImageHelper image)
        {
            List<WebStoreAttributeValueSwatchModel> list = new List<WebStoreAttributeValueSwatchModel>();
            foreach (AttributesSelectValuesModel item in swatches)
            {
                WebStoreAttributeValueSwatchModel model = new WebStoreAttributeValueSwatchModel
                {
                    Code = item.Code,
                    Value = item.Value,
                    ImageSmallThumbnailPath = image.GetImageHttpPathSmallThumbnail(item.Path),
                    ImageSmallPath = image.GetImageHttpPathSmall(item.VariantImagePath)
                };
                list.Add(model);
            }
            return list;
        }
        public virtual void GetpublishedConfigurableProducts(List<PublishProductModel> publishProducts, int PortalId, string attributeCode)
        {
            IImageHelper imageHelper = GetService<IImageHelper>();

            foreach (PublishProductModel publishProduct in publishProducts)
            {
                string productType = ZnodeConstant.ConfigurableProduct;
                if (IsConfigurableProduct(publishProduct, productType))
                {
                    List<AttributesSelectValuesModel> selectValues = GetSelectValues(attributeCode, publishProduct);
                    if (selectValues?.Count > 0)
                    {
                        publishProduct.AlternateImages = GetPublishSwatchImages(selectValues, imageHelper);
                    }
                }

            }
        }
        private bool IsConfigurableProduct(PublishProductModel publishProduct, string productType)
        {
            return publishProduct.Attributes.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.ProductType)?.SelectValues.FirstOrDefault().Code == productType;
        }
        private List<AttributesSelectValuesModel> GetSelectValues(string attributeCode, PublishProductModel publishProduct)
        {
            return publishProduct.Attributes.FirstOrDefault(x => x.AttributeCode.Equals(attributeCode, StringComparison.OrdinalIgnoreCase))?.SelectValues.OrderBy(x => x.VariantDisplayOrder).DistinctBy(z => z.Code).ToList();

        }
        private List<ProductAlterNateImageModel> GetPublishSwatchImages(List<AttributesSelectValuesModel> swatches, IImageHelper image)
        {
            List<ProductAlterNateImageModel> list = new List<ProductAlterNateImageModel>();
            var distinctColor = swatches.Select(x => x.Value).Distinct();

            foreach (var itemouter in distinctColor)
            {
                var item = swatches?.Where(x => x.Value == itemouter)?.FirstOrDefault();
                ProductAlterNateImageModel model = new ProductAlterNateImageModel
                {
                    FileName = item.Code,
                    ImageLargePath = item.Value,
                    ImageSmallThumbNail = image.GetImageHttpPathSmallThumbnail(item.Path),
                    ImageSmallPath = image.GetOriginalImagepath(item.VariantImagePath)
                };
                list.Add(model);
            }

            return list;
        }
    }
}
