﻿using Autofac;
using Znode.Api.Custom.Service;
using Znode.Api.Custom.Service.IService;
using Znode.Api.Custom.Service.Service;
using Znode.Api.Custom.ShoppingCart;
using Znode.Engine.Services;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Helper
{
    public class DependencyRegistration : IDependencyRegistration
    {
        /// <summary>
        /// Register the Dependency Injection types.
        /// </summary>
        /// <param name="builder">Autofac Container Builder</param>
        public virtual void Register(ContainerBuilder builder)
        {
            builder.RegisterType<DSCustomService>().As<IDSCustomService>().InstancePerRequest();
            builder.RegisterType<DSSearchService>().As<ISearchService>().InstancePerRequest();
            builder.RegisterType<DSAttributeSwatchHelper>().As<IAttributeSwatchHelper>().InstancePerDependency();
            builder.RegisterType<DSOrderService>().As<IOrderService>().InstancePerRequest();
            builder.RegisterType<DSWebstoreWidgetService>().As<IWebStoreWidgetService>().InstancePerRequest();
            builder.RegisterType<DSPublishProductService>().As<IPublishProductService>().InstancePerRequest();
            builder.RegisterType<DSPublishProductService>().As<IDSPublishProductService>().InstancePerRequest();
            builder.RegisterType<DSProductFeedService>().As<IProductFeedService>().InstancePerRequest();
            builder.RegisterType<DSShoppingCartService>().As<IShoppingCartService>().InstancePerRequest();
            builder.RegisterType<DSTouchPointConfigurationService>().As<ITouchPointConfigurationService>().InstancePerRequest();
            builder.RegisterType<DSUserService>().As<IUserService>().InstancePerRequest();
            builder.RegisterType<DSWebStoreCaseRequestService>().As<IWebStoreCaseRequestService>().InstancePerRequest();
            builder.RegisterType<DSZnodeOrderReceipt>().As<IZnodeOrderReceipt>().InstancePerRequest();
            //Here override znode base code method by injecting dependancy mention as below.
            //"In CustomPortalService.cs we have override 'DeletePortal()' of znode base code".
            //builder.RegisterType<CustomPortalService>().As<IPortalService>().InstancePerRequest();
        }

        /// <summary>
        /// Order method represents Dependency Injection Registration Order.
        /// For znode base code Library the DI registration order set to 0.
        /// For custom code library the DI registration order should be incremental.
        /// </summary>
        public int Order
        {
            get { return 1; }
        }
    }
}
