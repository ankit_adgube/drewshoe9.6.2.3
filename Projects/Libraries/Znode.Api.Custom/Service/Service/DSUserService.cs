﻿using System;
using System.Configuration;
using System.Web;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using CTCT;
using CTCT.Components;
using CTCT.Components.Contacts;
using CTCT.Services;
using Znode.Engine.Api.Models;
using Znode.Libraries.Framework.Business;
using System.Diagnostics;


namespace Znode.Api.Custom.Service.Service
{
    public class DSUserService : UserService
    {        
        private ConstantContactFactory _constantContactFactory = null;
        string _apiKey = ConfigurationManager.AppSettings["APIKey"];
        string _accessToken = ConfigurationManager.AppSettings["_accessToken"];
        string ListsIDros = ConfigurationManager.AppSettings["ListsIDros"];
        string ListsIDrew = ConfigurationManager.AppSettings["ListsIDrew"];
        string ListsIDBel = ConfigurationManager.AppSettings["ListsIDBel"];
        string ListsIDDel = ConfigurationManager.AppSettings["ListsIDDel"];
        string ListsIDpenny = ConfigurationManager.AppSettings["ListsIDpenny"];
        string PortalIdDrew = ConfigurationManager.AppSettings["PortalIDDrew"];
        string PortalIdRos = ConfigurationManager.AppSettings["PortalIDRos"];
        string PortalIdBellini = ConfigurationManager.AppSettings["PortalIDBellini"];
       
        string firstname;
        string lastname;
        #region Constructor

        public DSUserService() : base()
        {            
            string code = HttpContext.Current.Request.QueryString["code"];
            if (!string.IsNullOrWhiteSpace(code))
            {
                string state = "ok";
                _accessToken = "ac19f684-5a8b-47b1-862c-ef659ba8c72c";
            }
            IUserServiceContext userServiceContext = new UserServiceContext(_accessToken, _apiKey);
            ContactService contactService = new ContactService(userServiceContext);
        }

        #endregion Constructor


        public override bool SignUpForNewsLetter(NewsLetterSignUpModel model)
        {
            string str = model.Email;
            string[] seperatestring = str.Split(new char[] { ',' });
            model.Email = seperatestring[0];
            firstname = seperatestring[1];
            lastname = seperatestring[2];

            OAuth.AuthorizeFromWebApplication(HttpContext.Current, "ok");
            IUserServiceContext userServiceContext = new UserServiceContext(_accessToken, _apiKey);
            _constantContactFactory = new ConstantContactFactory(userServiceContext);

            bool Isvalid = base.SignUpForNewsLetter(model);

            if (Isvalid)
            {
                try
                {
                    GetNewsLetterCampaign(model.Email);
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage(ex, "DSUserService:SignUpForNewsLetter ", TraceLevel.Error);
                }
            }
            return Isvalid;
        }
        #region Constant Contact
        public void GetNewsLetterCampaign(string Email)
        {
            //get contact if exists (by email address)
            CTCT.Components.Contacts.Contact contact = null;
            contact = GetContactByEmailAddress(Email);

            bool alreadyExists = contact != null ? true : false;

            contact = UpdateContactFields(contact, Email);

            CTCT.Components.Contacts.Contact result = null;
            var contactService = _constantContactFactory.CreateContactService();

            result = contactService.AddContact(contact, false);
            if (result != null)
            {
                if (alreadyExists)
                {
                    //messageResult = "Changes successfully saved!";
                }
                else
                {
                    //messageResult = "Contact successfully added!";
                }
            }
        }
        private CTCT.Components.Contacts.Contact GetContactByEmailAddress(string emailAddress)
        {
            var contactService = _constantContactFactory.CreateContactService();
            ResultSet<CTCT.Components.Contacts.Contact> contacts = contactService.GetContacts(emailAddress, 1, DateTime.Now, null);

            if (contacts != null)
            {
                if (contacts.Results != null && contacts.Results.Count > 0)
                {
                    return contacts.Results[0];
                }
            }
            return null;
        }
        private CTCT.Components.Contacts.Contact UpdateContactFields(CTCT.Components.Contacts.Contact contact, string Email)
        {
            if (contact == null)
            {
                contact = new CTCT.Components.Contacts.Contact();
                //add lists [Required]
                if (PortalId == Convert.ToInt32(PortalIdDrew))
                {
                    contact.Lists.Add(new ContactList() { Id = ListsIDrew, Status = Status.Active });
                }
                else if (PortalId == Convert.ToInt32(PortalIdRos))
                {
                    contact.Lists.Add(new ContactList() { Id = ListsIDros, Status = Status.Active });
                }
                else if (PortalId == Convert.ToInt32(PortalIdBellini))
                {
                    contact.Lists.Add(new ContactList() { Id = ListsIDBel, Status = Status.Active });
                }
                //contact.Lists.Add(new ContactList() { Id = ListsID, Status = Status.Active });

                //add email_addresses [Required]
                var emailAddress = new EmailAddress()
                {
                    Status = Status.Active,
                    ConfirmStatus = ConfirmStatus.NoConfirmationRequired,
                    EmailAddr = Email.Trim()
                };
                contact.EmailAddresses.Add(emailAddress);
            }
            contact.Status = Status.Active;
            contact.FirstName = firstname.Trim();
            contact.LastName = lastname.Trim();
            return contact;
        }
        #endregion Constant Contact    


    }
}
