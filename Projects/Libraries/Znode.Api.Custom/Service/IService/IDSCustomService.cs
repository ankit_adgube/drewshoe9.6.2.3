﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Expands;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Sample.Api.Model;
using Znode.Sample.Api.Model.Responses;

namespace Znode.Api.Custom.Service.IService
{
    public interface IDSCustomService
    {
        DSCustomListModel GetInventoyGrid(int PublishProductId, string Color);
    }


}
