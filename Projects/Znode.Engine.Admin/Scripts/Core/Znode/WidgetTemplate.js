var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var WidgetTemplate = /** @class */ (function (_super) {
    __extends(WidgetTemplate, _super);
    function WidgetTemplate() {
        return _super.call(this) || this;
    }
    WidgetTemplate.prototype.DeleteWidgetTemplate = function (control) {
        var widgetTemplateId = DynamicGrid.prototype.GetMultipleSelectedIds();
        var fileName = DynamicGrid.prototype.GetMultipleValuesOfGridColumn('File Name');
        if (widgetTemplateId.length > 0) {
            Endpoint.prototype.DeleteWidgetTemplate(widgetTemplateId, fileName, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    };
    WidgetTemplate.prototype.ValidateTemplateCode = function () {
        var isValid = true;
        var widgetTemplateId = $('#WidgetTemplateId').val();
        if (widgetTemplateId < 1 && $("#Code").val() != '') {
            Endpoint.prototype.IsWidgetTemplateExist($("#Code").val(), function (response) {
                if (response.data) {
                    $("#Code").addClass("input-validation-error");
                    $("#valCode").addClass("error-msg");
                    $("#valCode").text(ZnodeBase.prototype.getResourceByKeyName("AlreadyExistWidgetTemplate"));
                    $("#valCode").show();
                    isValid = false;
                    ZnodeBase.prototype.HideLoader();
                }
            });
        }
        return isValid;
    };
    return WidgetTemplate;
}(ZnodeBase));
//# sourceMappingURL=WidgetTemplate.js.map